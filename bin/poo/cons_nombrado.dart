void main(List<String> args) {
  String especie = 'caballo';
  String tipo = 'mamifero';
  //instancia de animales
  //Animales animal = new Animales(nombre: 'Aguila', tipo: 'Ave');

  var datos ={
    'nombre' : '${especie}',
    'tipo' : '${tipo}'
  };

  Animales animal =  Animales.fromJson(datos);

  print('Animal: ${animal.nombre} - ${animal.tipo} ');
}

class Animales {
  String nombre;
  String tipo;

//constructor

  // Animales({String nombre, String tipo = ''}) {
  //   this.nombre = nombre;
  //   this.tipo = tipo;
  // }

  //primero declaramos este para luego crear la instancia
Animales({ this.nombre, this.tipo });

Animales.fromJson( Map jsonMap ){
  nombre = jsonMap['nombre'];
  tipo = jsonMap['tipo'];

}

}
