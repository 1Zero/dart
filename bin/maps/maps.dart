void main(List<String> args){

  Map<int, String>mapAlumno = {1:'Jorge', 2:'Ubuntu', 3:'Linux'};

  mapAlumno.forEach((key, alumno){
  print('$key $mapAlumno');
});

mapAlumno.update(2, (alumno) => 'linuxmint');
print(mapAlumno);

mapAlumno.remove(1);
print(mapAlumno);

mapAlumno.removeWhere((key, alumno) => alumno =='ubuntu');
print(mapAlumno);

}