void main(){
    var myList = [52, 6, 87];
 
    try {
        for(var i=0;i<10;i++) {
            print(myList[i]);
        }
    } on RangeError {
        print('Estás saliendo del rango de la lista. Verifique el rango que está considerando ');
    } catch (e) {
        print('Se produjo una excepción desconocida al imprimir la lista ');
    } finally {
        print('Este bloque finalmente se ejecutará.');
    }
    print('Continuando con el resto del programa. ');
}