import 'constantes_nombrados2.dart';

void main(List<String> args) {
  Doctor medico1 = new Doctor();
  medico1._cedula = 'DSDS234232';
  String nombre = 'Albaro';
  String apellido = 'axiola';
  String especialidad = 'nefrologia';

  var json={
    'nombre' : '${nombre}',
    'apellido' : '${apellido}',
    'especialidad' : '${especialidad}'
  };

  Doctor medico = new Doctor.fromJson(json);

  print('cedula: ${medico1._cedula} - nombre: ${medico.nombre} - apellido: ${medico.apellido} - especialidad: ${medico.especialidad} ');
  
}

class Doctor{
  String _cedula;
  String especialidad;
  String nombre;
  String apellido;

  set cedula(String cedula)=>this._cedula =cedula;

  Doctor( {this.nombre, this.apellido, this.especialidad} );

  Doctor.fromJson( Map jsonMap ){
    nombre = jsonMap['nombre'];
    apellido = jsonMap['apellido'];
    especialidad = jsonMap['especialidad'];
  }


}