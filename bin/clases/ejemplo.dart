void main(List<String> args) {
  //instancia de animales
  Animales animal = new Animales(nombre: 'Aguila', tipo: 'Ave');

  print('Animal: ${animal.nombre} - ${animal.tipo} ');
  
}

class Animales{
  String nombre;
  String tipo;

//constructor

  Animales({String nombre, String tipo = ''}){
    this.nombre = nombre;
    this.tipo = tipo;

  }
}